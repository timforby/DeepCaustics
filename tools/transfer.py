import cv2
import c_t
import h_m
import os

src = cv2.imread("set2_color_0002.png")
loc = "./dataset_2"
rfa = loc+"_ct"
if not os.path.exists(rfa):
    os.mkdir(rfa)
for file in os.listdir(loc):
    trg = cv2.imread(loc+"/"+file)
    res = c_t.color_transfer(src,trg)
    res = h_m.lum_match(res,trg)
    cv2.imwrite(rfa+"/"+file.split(".")[0]+".png", res)
