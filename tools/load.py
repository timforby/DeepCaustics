import scipy
import os
import cv2
import numpy as np
from random import uniform



# Walk through path and get file names as strings
def get_filenames(path):
    f_names = []
    for (dir_path, dir_names, file_names) in os.walk(path):
        f_names.extend(file_names)
        break
    return f_names


# Load images in given directory
def load_imgs(fnames, dir_name, channels, img_shape, num_imgs=199, grey=False, real=False):
    if channels == 1:
        grey = True

    imgs = []
    count = 0
    for img in fnames:
        if count > num_imgs:
            break
        count += 1
        fullpath = os.path.join(dir_name, img)
        img = scipy.misc.imread(fullpath, grey, 'RGB')

       
        if grey and len(img.shape) == 2:
            img = img.reshape(img.shape[0], img.shape[1], 1)

        if grey and img.shape[2] == 3:
            img = np.dot(img[..., :3], [0.2989, 0.587, 0.114])

        elif grey and img.shape[2] == 1:
            img = np.tile(img,3)

        if img.shape[0] > img_shape[0] or img.shape[1] > img_shape[1]:
            img = scipy.misc.imresize(img, img_shape)

        imgs.append(img.reshape(-1))

    return imgs


# Returns data to be used in the training process
# To be returned:
#   trX: training set of synthetic images
#   trY: ground truths for trX
#   teX: validation set of synthetic images
#   teY: ground truths for teX
#   teReal : sample images extracted from a real video
def get_data(channels, img_shape=(100, 100)):
    channels = 3
    img_x = img_shape[0]
    img_y = img_shape[1]

    # -----------Training images set 1 (generally 3D created)------------
    # Path
    train_img_path = os.path.join("..","data",
       "datasetv2",
       "Underwater_Caustics",
       "set2_old")
    train_mask_path = os.path.join("..","data",
      "datasetv2",
      "Mask",
      "set2_actual_thresholded_mask")
    truth_img_path = os.path.join("..","data",
      "datasetv2",
      "Mask",
      "set2_ground_truth")

    #Image names np.array(
    train_img_name = sorted(get_filenames(train_img_path))
    train_mask_name = sorted(get_filenames(train_mask_path))
    truth_img_name = sorted(get_filenames(truth_img_path))
    #Images as np array float32
    train_imgs = np.array(load_imgs(train_img_name, train_img_path, channels, img_shape=img_shape)).astype(np.float32)/255.0
    train_msks = np.array(load_imgs(train_mask_name, train_mask_path, channels, img_shape=img_shape)).astype(np.float32)/255.0
    truth_imgs = np.array(load_imgs(truth_img_name, truth_img_path, channels, img_shape=img_shape)).astype(np.float32)/255.0
    train_imgs = train_imgs.reshape((train_imgs.shape[0], img_x, img_y, channels))
    train_msks = train_msks.reshape((train_msks.shape[0], img_x, img_y, channels))
    truth_imgs = truth_imgs.reshape((truth_imgs.shape[0], img_x, img_y, channels))
    
    train_imgs = np.concatenate((train_imgs,train_msks),axis=3)
    train_imgs = train_imgs[:,:,:,:4]
    truth_imgs = np.concatenate((truth_imgs,np.ones((truth_imgs.shape[0], img_x, img_y, channels)).astype(np.float32)), axis=3)
    truth_imgs = truth_imgs[:,:,:,:4]

    # --------------------Reshape data into the format theano wants it for convolutions
    #  Rreshape((trY.shape[0], img_x, img_y, channels))
    train_imgs = np.swapaxes(train_imgs, 2, 3)
    train_imgs = np.swapaxes(train_imgs, 1, 2)

    truth_imgs = np.swapaxes(truth_imgs, 2, 3)
    truth_imgs = np.swapaxes(truth_imgs, 1, 2)

    return train_imgs, truth_imgs
